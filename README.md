## Rest API
### CRUD Methods
Create - HTTP POST
Read - HTTP GET
Update - HTTP PUT
Delete - HTTP DELETE

Request parameter is what the client is passing into our REST API

Response parameter is what we use to send a response.

Next parameter is something we use for middleware

### HTTP Methods in REST
POST (Create new)
GET (Get existing)
PUT (Update exisiting)
DELETE

